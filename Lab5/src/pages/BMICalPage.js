import BMIResult from "../components/BMIResult";
import { useState } from "react";
import Button from '@mui/material/Button';
import  Grid  from '@mui/material/Grid';
import { Typography,Box,Container } from "@mui/material";

function BMICalPage(){

    const [ name , setName ] = useState();
    const [ bmiresult, setBmiresult ] = useState(0);
    const [ translateResult , setTranslateResult ] = useState("");
    const [ height, setHeight ]  =  useState("");
    const [ weight, setWeight ]  =  useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi =  w / (h*h) ;
        setBmiresult(bmi );
        if (bmi > 25 ){
            setTranslateResult("bigger")
        }else{
            setTranslateResult("lower")
        }
    }
    return (
        <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{ marginTop : "10px"}}>
                <Grid item xs={12}>
                    <Typography variant="h5">
                    ยินดีตอนรับเข้าสู้เว็บคำนวณ
                    </Typography>
                </Grid>
                <Grid item xs={8} >
                    <Box sx={{ textAlign  : 'left'}}>
                <br />
                    คุณชื่อ : <input type="text"
                                    value={ name }
                                    onChange= {  (e) => {setName(e.target.value); }}/> 
                                    <br />
                                    <br />
                    สูง : <input type="text"
                                    value={ height }
                                    onChange= {  (e) => {setHeight(e.target.value); }} /> 
                                    <br />
                                    <br />
                    หนัก : <input type="text"
                                    value={ weight }
                                    onChange= {  (e) => {setWeight(e.target.value); }} /> 
                                    <br />
                                    <br />
                    
                    <Button variant="contained" onClick={ calculateBMI }>Calculate</Button>
                    </Box>
                </Grid>
                <Grid item xs={4}>
                    { bmiresult != 0 && 
                            <div>
                        <hr />
                        นี้คือผลการคำนวณ
                        <BMIResult
                                name = { name }
                                bmi = { bmiresult } 
                                result = { translateResult }/>
                                </div>
                        }
                </Grid>
            </Grid>
        </Container>
    );
}

export default BMICalPage ;






      /*  <div align="left">
            <div align="center">
                ยินดีตอนรับเข้าสู้เว็บคำนวณ BMI
             <br />
                คุณชื่อ : <input type="text"
                                value={ name }
                                onChange= {  (e) => {setName(e.target.value); }}/> <br />
                สูง : <input type="text"
                                value={ height }
                                onChange= {  (e) => {setHeight(e.target.value); }} /> <br />
                หนัก : <input type="text"
                                value={ weight }
                                onChange= {  (e) => {setWeight(e.target.value); }} /> <br />
                
                <Button variant="contained" onClick={ calculateBMI }>Calculate</Button>

                { bmiresult != 0 && 
                    <div>
                <hr />
                นี้คือผลการคำนวณ
                <BMIResult
                        name = { name }
                        bmi = { bmiresult } 
                        result = { translateResult }/>
                        </div>
                }
            </div>
        </div>*/