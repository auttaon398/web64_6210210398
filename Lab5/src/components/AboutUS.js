import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box'
function AboutUS (props){

    return ( 
        <Box sx={ { width : "60%" } }>
        <Paper elevation={3}>
            <h1>จัดทำโดย : { props.name }</h1>
            <h2>ติดต่อได้ที่ :{ props.address } </h2>
            <h3>บ้านอยู่ จังหวัด { props.province } </h3>
         </Paper>
         </Box>
    );
}

export default AboutUS;