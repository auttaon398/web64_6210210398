
function AboutUS (props){

    return ( 
        <div>
            <h1>จัดทำโดย : { props.name }</h1>
            <h2>ติดต่อได้ที่ :{ props.address } </h2>
            <h3>บ้านอยู่ จังหวัด { props.province } </h3>
         </div>
    );
}

export default AboutUS;