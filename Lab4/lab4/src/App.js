import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import BMICalPage from './pages/BMICalPage';
import Header from './components/Header';

import { Routes, Route, Link } from "react-router-dom";
import LuckyNumberPage from './pages/LuckyNuberPage';

function App() {
  return (
    <div className="App" id='BG'>
      <Header />
      <Routes>
        <Route path="about" element={
               <AboutUsPage />
        } /> 
       <Route path="/" element={
              <BMICalPage />
        } /> 
        <Route path="+" element={
              <LuckyNumberPage />
        } /> 
        </Routes>
    </div>
  );
}

export default App;
