import React, { useState } from 'react'

function LuckyNumberPage ( ) {

    const [username,setUsername] = useState()
    const [luckyNumber,setLuckyNumber] = useState(8)
    const [translucky , setTranslucky ] = useState("");
    
    React.useEffect(()=>{
        setTimeout(()=>{
          setUsername("Hello Guy")
        })})
      
    function lucky (){  
        if (luckyNumber == 69 ){
            setTranslucky("ถูกเเล้วจ๊าาา")
        }else{
            setTranslucky("ผิด!!!!")
        }
    }
    return (
      <div >
        {!username ? <div>Loading...</div> : <h1> {username}!</h1>}
              <h2> Your lucky number 0-99 is {luckyNumber}</h2>    
              <button onClick={()=>{setLuckyNumber(Math.floor(Math.random()*100))}} >Get New Lucky Number</button>
        <br />  
            <div>
                  <h3> คำตอบ : { translucky }</h3>
                  <button onClick={ lucky }> ทาย </button>
            </div>
      </div>
    );
}
export default LuckyNumberPage;