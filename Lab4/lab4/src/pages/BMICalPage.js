import BMIResult from "../components/BMIResult";
import { useState } from "react";

function BMICalPage(){

    const [ name , setName ] = useState();
    const [ bmiresult, setBmiresult ] = useState(0);
    const [ translateResult , setTranslateResult ] = useState("");

    const [ height, setHeight ]  =  useState("");
    const [ weight, setWeight ]  =  useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi =  w / (h*h) ;
        setBmiresult(bmi );
        if (bmi > 25 ){
            setTranslateResult("bigger")
        }else{
            setTranslateResult("lower")
        }
    }
    return (

        <div align="left">
            <div align="center">
                ยินดีตอนรับเข้าสู้เว็บคำนวณ BMI
             <br />
                คุณชื่อ : <input type="text"
                                value={ name }
                                onChange= {  (e) => {setName(e.target.value); }}/> <br />
                สูง : <input type="text"
                                value={ height }
                                onChange= {  (e) => {setHeight(e.target.value); }} /> <br />
                หนัก : <input type="text"
                                value={ weight }
                                onChange= {  (e) => {setWeight(e.target.value); }} /> <br />
                
                <button onClick={ calculateBMI }> Calculate </button>
                { bmiresult != 0 && 
                    <div>
                <hr />
                นี้คือผลการคำนวณ
                <BMIResult
                        name = { name }
                        bmi = { bmiresult } 
                        result = { translateResult }/>
                        </div>
                }
            </div>
        </div>
    );
}

export default BMICalPage ;




