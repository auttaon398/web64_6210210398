const express = require('express')
const app = express()
const port = 4000


app.get('/', (req, res) => {
    res.send('Hello world!')
})

app.post('/bmi', (req,res) => {

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if( !isNaN(weight) && !isNaN(height)) {
        let bmi = weight / (height * height)
        result = {
            "status" : 200,
            "bmi"  : bmi
            }
        }else {

            result = {
                "status" : 400,
                "message" : "weight or height is not a number "
            }
        }

        res.send(JSON.stringify(result))
})

app.get('/triangle', (req,res) => {

    let base = parseFloat(req.query.base)
    let height = parseFloat(req.query.height)
    var result = {}

    if( !isNaN(base) && !isNaN(height)) {
        let triangle = base * height
        result = {
            "status" : 200,
            "triangle"  : triangle
            }
        }else {

            result = {
                "status" : 400,
                "message" : "base or height is not a number "
            }
        }

        res.send(JSON.stringify(result))
})

app.post('/score', (req,res) => {

    let score = parseFloat(req.query.score)
    var result = {}
    

    
    if( !isNaN(score)) {
        let Grade
        if (score < 60){
            Grade = " E"  
            } 
          else if (score < 70) {
            Grade ="D"
        } 
          else if (score  < 80) {
            Grade = "C"
        } else if (score < 90) {
            Grade = "B"
          } else if (score < 100) {
            Grade= "A"
  }

        result = {
            "status" : 200,
            "student"  : req.query.name +" Grade "+ Grade
            }
        }

        res.send(JSON.stringify(result))
})
  
app.get('/hello', (req, res) => {
  res.send('Sawasdee khun '+req.query.name)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})