const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection =mysql.createConnection({
    host : 'localhost',
    user : 'runner_admin',
    password : 'runner_admin',
    database : 'RunningSystem'
})

connection.connect();
const express = require('express');
const app = express()
const port = 4000

/* Middleware for Authenicating User Token */
function authenticateToken(req,res,next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET, (err,user) => {
        if(err) { return res.sendStatus(403) }
        else{
            req.user = user
            next()
        }
    })
}

/*Query 1 : list all registrations*/
app.get("/list_registrations", (req,res) => {
    let query = `SELECT
                    Runner.RunnerID,
                    Runner.RunnerName,
                    Runner.RunnerSurname,
                    RunningEvent.EventName,
                    Regitstration.Distance,
                    Regitstration.RegitstrationTime
                FROM
                    Runner,
                    RunningEvent,
                    Regitstration
                WHERE
                    (Regitstration.RunnerID =  Runner.RunnerID)
                    AND
                    (Regitstration.EventID =  RunningEvent.EventID)`;
        connection.query( query, (err,rows) => {
            if(err){
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
            }else{
                res.json(rows)
            }
        })
})

 /*Query 2 : list all registrations by running event */   

app.get("/list_reg_event", (req,res) => {
    let event_id = req.query.event_id
    let query = `SELECT
                    Runner.RunnerID,
                    Runner.RunnerName,
                    Runner.RunnerSurname,
                    Regitstration.Distance,
                    Regitstration.RegitstrationTime
                FROM
                    Runner,
                    RunningEvent,
                    Regitstration
                WHERE
                    (
                        Regitstration.RunnerID = Runner.RunnerID
                    ) AND(
                        Regitstration.EventID = RunningEvent.EventID
                    ) AND
                        (Regitstration.EventID = ${event_id})`;
        connection.query( query, (err,rows) => {
            if(err){
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
            }else{
                res.json(rows)
            }
        })
})

/*


        Query 3 : list all registrations by running id

*/

app.get("/list_reg_runnerid", authenticateToken , (req,res) => {

    let runner_id = req.user.user_id
    if (!req.user.IsAdmin) {res.send( "Unauthorized Because you're noi admin")}
    else{
        let query = `SELECT
                        Runner.RunnerID,
                        RunningEvent.EventName,
                        Regitstration.Distance,
                        Regitstration.RegitstrationTime
                    FROM
                        Runner,
                        RunningEvent,
                        Regitstration
                    WHERE
                        (
                            Runner.RunnerID = Regitstration.RunnerID
                        ) AND(
                            Regitstration.EventID = RunningEvent.EventID
                        ) AND
                            (Runner.RunnerID = ${runner_id})`;
            connection.query( query, (err,rows) => {
                if(err){
                    console.log(err)
                    res.json({
                                "status" : "400",
                                "message" : "Error querying from running db"
                            })
                }else{
                    res.json(rows)
                }
            })
     }
})




// API for registering a new Running Event
app.post("/register_event", authenticateToken ,(req, res) => {
  
    let runner_id = req.user.user_id
    let event_id = req.query.event_id
    let distance = req.query.distance
    let query= `INSERT INTO Regitstration (RunnerID, EventID , Distance ,RegitstrationTime) VALUES ( 
                                                                     '${runner_id}' , 
                                                                     '${event_id}',
                                                                     '${distance}',
                                                                      NOW())`
    console.log(query)
    connection.query( query, (err,rows) => {
        if(err){
            res.json({
                         "status" : "400",
                         "message" : "Error inserting datainto db"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding event succesful"
                     })
                 }
        })
})

// API for Processing Runner Authorization
app.post("/login", (req,res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query =`SELECT * FROM Runner WHERE Username='${username}'`
    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error inserting datainto db"
                    })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err,result) => {
                if (result){
                  let payload = {
                      "username" : rows[0].Username,
                      "user_id" : rows[0].RunnerID,
                      "IsAdmin" : rows[0].IsAdmin
                  }
                  console.log(payload)
                  let token = jwt.sign(payload,TOKEN_SECRET,{ expiresIn : '1d'})
                  res.send(token)
                }else { res.send("Tnvalid username/password")}
            })
        }
    })
})





// API for register a new Runner 

app.post("/register_runner", (req, res) => {

    let runner_name = req.query.runner_name
    let runner_surname = req.query.runner_surname
    let runner_username = req.query.runner_username
    let runner_password = req.query.runner_password


    bcrypt.hash(runner_password, SALT_ROUNDS,(err,hash) => {

        let query= `INSERT INTO Runner (RunnerName , RunnerSurname, Username ,Password ,IsAdmin) 
                    VALUES ( '${runner_name}' , '${runner_surname}' , '${runner_username}', '${hash}',false)`
        console.log(query)
    
        connection.query( query, (err,rows) => {
            if(err){
                console.log(query)
                res.json({
                             "status" : "400",
                             "message" : "Error inserting datainto db"
                         })
            }else{
                console.log(query)
                res.json({
                            "status" : "200",
                            "message" : "Adding new user succesful"
                         })
                     }
            })  
    })  
})

 
// CRUD Operation for Running Table 
app.get("/list_event", (req,res) => {
    let query = "SELECT * FROM RunningEvent";
    connection.query( query, (err,rows) => {
            if(err){
                res.json({
                             "status" : "400",
                             "message" : "Error querying from running db"
                         })
            }else{
                res.json(rows)
            }
    })
})

app.post("/add_event", (req, res) => {
    
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query= `INSERT INTO RunningEvent (EventName , EventLocation) VALUES ( '${event_name}' , '${event_location}')`
    console.log(query)

    connection.query( query, (err,rows) => {
        if(err){
            res.json({
                         "status" : "400",
                         "message" : "Error inserting datainto db"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding event succesful"
                     })
                 }
        })
})


app.post("/update_event", (req, res) => {
    
    let event_id = req.query.event_id
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query= `UPDATE  RunningEvent SET EventName='${event_name}', EventLocation='${event_location}' WHERE EventID=${event_id}`
    console.log(query)

    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error update record"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding update succesful"
                     })
                 }
        })
})



app.post("/delete_event", (req, res) => {
    
    let event_id = req.query.event_id

    let query= `DELETE FROM RunningEvent  WHERE EventID=${event_id}`
    console.log(query)

    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error delete record"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding delete succesful"
                     })
                 }
        })
})

app.listen(port, () => {
    console.log(`Now stating Running System Backend ${port}`)
})


/*
query = "SELECT * from Runner";
connection.query( query, (err,rows) =>{
        if(err){
            console.log(err);
        }else{
            console.log(rows);
        }
});

connection.end();*/